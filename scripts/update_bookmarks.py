#!/usr/bin/env python3

import datetime
import operator
import re
import sys
import time

import click
import feedparser
import ruamel.yaml

feed_url = r"https://feeds.pinboard.in/rss/u:beyondseven/"


@click.command()
@click.argument("existing_bookmarks_filename")
def update_bookmarks(existing_bookmarks_filename):
    """Update in-place existing bookmarks with any new bookmarks.

    The goal is to add new bookmarks without altering existing ones even if
    newer versions exist on the bookmarking site. The reasoning here is that the
    existing bookmarks are archival versions of bookmarks at a particular point
    in time.

    """
    yaml = ruamel.yaml.YAML(typ="safe")
    yaml.explicit_start = True
    with open(existing_bookmarks_filename) as fh:
        existing_entries = list(yaml.load_all(fh))
    existing_ids = {item["id"] for item in existing_entries}
    assert len(existing_entries) == len(existing_ids)
    d = feedparser.parse(feed_url)
    assert d.feed.title == "Pinboard (beyondseven)"
    new_entries = []
    for entry in d.entries:
        id = re.search(r"/b:([0-9a-f]+)/", entry["dc_identifier"]).group(1)
        if id in existing_ids:
            continue
        tags = entry["tags"].pop()["term"].split(" ")
        assert "summary" in entry, entry
        date = datetime.datetime.fromtimestamp(time.mktime(entry["updated_parsed"]))
        # create a copy of new_issue_dt, otherwise ruamel.yaml will create an anchor
        modified = date + datetime.timedelta(days=0)
        assert any('!' in tag for tag in tags), (id, tags)
        new_entries.append(
            {
                "id": id,
                "url": entry["link"],
                "title": entry["title"],
                "comment": entry["summary"],
                "date": date,
                "modified": modified,
                "tags": tags,
            }
        )
    entries = existing_entries + sorted(new_entries, key=operator.itemgetter("date"))
    with open(existing_bookmarks_filename, "w") as fh:
        yaml.dump_all(entries, fh)
    print(
        f"added {len(new_entries)} new {'entry' if len(new_entries) == 1 else 'entries'}.",
        file=sys.stderr,
    )


if __name__ == "__main__":
    update_bookmarks()
