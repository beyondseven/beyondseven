# Blog render target for Beyond Seven

**No content lives in this directory.**  This directory only
contains files used to "render" content found elsewhere as a blog.

## Quickstart

1. Install dokku
2. Install dokku plugins `letsencrypt`
3. Create the app and sync it with the remote repository:
    ```bash
    dokku apps:create beyondseven
    dokku git:sync beyondseven https://gitlab.com/beyondseven/beyondseven.git
    dokku ps:rebuild beyondseven
    
    dokku domains:set beyondseven www.beyondseven.org
    dokku letsencrypt:enable beyondseven
    ```
4. Install `cronic`
5. Add the following crontab entry to the `root` user's crontab
    ```crontab
    0 1 * * 3 cronic bash -c "docker image prune -f && yes | sudo -u dokku dokku git:unlock beyondseven && sudo -u dokku dokku git:sync --build beyondseven https://gitlab.com/beyondseven/beyondseven.git 2> /dev/null"
    ```

Note: using `git:from-archive` instead of `git:sync` was not working for some
reason in late 2021. Not sure why. If it could be made to work, the crontab
entry could be simplified.
