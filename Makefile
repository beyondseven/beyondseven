default: update_issues

update_bookmarks:
	@python3 scripts/update_bookmarks.py source/bookmarks.yaml

update_issues: update_bookmarks
	# the git diff-index command checks to see if there are any staged commits and exits with 1 if there are
	@python3 scripts/update_beyond_seven_review_issues.py source/beyond-seven-review-issues.yaml source/bookmarks.yaml && \
		git diff-index --quiet --cached HEAD -- && \
		git add source/beyond-seven-review-issues.yaml source/bookmarks.yaml && \
		git commit -m"feat: update bookmarks" && \
		git push
