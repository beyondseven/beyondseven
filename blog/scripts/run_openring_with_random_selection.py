import sys
import subprocess
import random

BLOG_FEEDS = [
    "https://dragonfly.hypotheses.org/feed",
    "http://brewster.kahle.org/feed/",
    "https://blog.archive.org/feed/",
    "https://scholarslab.lib.virginia.edu/feed.xml",
    "http://bactra.org/weblog/index.rss",
    "https://simblob.blogspot.com/feeds/posts/default",
    "https://dhd-blog.org/?feed=rss2",
    "https://languagelog.ldc.upenn.edu/nll/?feed=atom",
]


def main(openring_binary_filename, input_template_filename):
    random.shuffle(BLOG_FEEDS)
    # openring will only use 3 blog feeds by default
    openring_args = []
    for feed_url in BLOG_FEEDS[:3]:
        openring_args.append('-s')
        openring_args.append(feed_url)
    with open(input_template_filename, "r") as fh:
        stdin_content = fh.read()
    args = [openring_binary_filename] + openring_args
    completed_process = subprocess.run(
        args,
        input=stdin_content,
        text=True,
        capture_output=True,
    )
    print(completed_process.stdout)


if __name__ == "__main__":
    if len(sys.argv) != 3:
        print("usage: script.py /path/to/openring in.html")
    main(sys.argv[1], sys.argv[2])

#~/go/bin/openring -s https://drewdevault.com/feed.xml \
#      -s https://emersion.fr/blog/rss.xml \
#      -s https://danluu.com/atom.xml \
#      -s https://dragonfly.hypotheses.org/feed < misc/openring_template.html > /tmp/out.html
