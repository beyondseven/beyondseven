# Adding New Entries

## Candidate URLs Collected

Candidate URLs are collected in a bookmark manager, `buku`. This happens continuously.

# Candidate URLs Reviewed

Collected URLs are reviewed periodically. Those which still seem interesting after a week or two has passed are published.

The following bash snippet is used to pick 4 URLs at random and store their buku record ids:

    RECORDS=$(buku --json --deep --sany '!important-' | jq -c '.[] | {index: .index, tags: .tags}' | grep -v reviewed-for-beyond | shuf | head -n 4 | sort | jq .index | xargs echo)

Each id is then marked as reviewed:

    for id in $(echo $RECORDS); do buku --update $id --tag + '!reviewed-for-beyond-seven-review'; done

Any URLs which seem interesting are added to the `beyondseven` Pinboard account, typically with some kind of comment.

## Entries Published

Run `make` in the root directory of the repository, commit changes, and push.

```
make && git add source/ && git commit -m"feat: update bookmarks" && git push
```
