# This Dockerfile renders issues as a blog.
#
# It belongs in the `blog` directory but dokku requires
# that this file be in the root of the repository.
#
FROM golang:1.15-buster

RUN go get -v git.sr.ht/~sircmpwn/openring

#---------------------------------------------------------------------------
FROM python:3.9-slim-buster

COPY blog /app
COPY source/bookmarks.yaml /app
COPY source/beyond-seven-review-issues.yaml /app

WORKDIR /app


RUN python3 -m pip install -r requirements.txt
RUN python3 scripts/generate_beyond_seven_review_posts.py beyond-seven-review-issues.yaml bookmarks.yaml /app/content

# openring
COPY --from=0 /go/bin/openring /app
RUN ls -alh /app/openring
RUN python3 scripts/run_openring_with_random_selection.py /app/openring misc/openring_template.html > /app/openring_output.html
RUN for filename in /app/content/beyond-seven-review-issue-*.md; do /bin/echo -e "\n\n<hr>\n\n" >> $filename; cat /app/openring_output.html >> $filename; done

RUN PYTHONPATH=/app pelican -v --settings publishconf.py

#---------------------------------------------------------------------------
FROM nginx:1.15

RUN rm -rf /usr/share/nginx/html
COPY --from=1 /app/output /usr/share/nginx/html
