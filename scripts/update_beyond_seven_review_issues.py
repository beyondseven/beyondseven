#!/usr/bin/env python3
"""Print Beyond Seven Review issues with new entries added."""

import datetime
import operator
import re
import secrets
import sys
import time

import click
import ruamel.yaml

# parameters unlikely to change frequently
START_DATE = "2018-09-25"  # REMOVE after first issue
DAYS_BETWEEN_ISSUES = 14
ISSUE_ID_BITS = 32  # issue identifiers are random n-bit integers
MAX_BOOKMARKS_PER_ISSUE = 4
MIN_BOOKMARKS_PER_ISSUE = 4
BLACKLISTED_TAGS = ["!paywalled"]

@click.command()
@click.argument("issues_filename")
@click.argument("bookmarks_filename")
def update_beyond_seven_review_issues(issues_filename, bookmarks_filename):
    """Update in-place Beyond Seven Review issues with new entries added.

    Does not change any existing entries. Add at most `MAX_BOOKMARKS_PER_ISSUE`
    bookmarks per issue. Prints results to stdout.

    Args:
        issues_filename: YAML file containing past issues of Beyond Seven Review
        bookmarks_filename: YAML file containing bookmarks, source of new entries

    """
    yaml = ruamel.yaml.YAML(typ="safe")
    yaml.explicit_start = True
    with open(issues_filename) as fh:
        published_issues = list(yaml.load_all(fh))
    published_ids = {item["id"] for item in published_issues}
    published_bookmark_ids = sum(
        [item["bookmark_ids"] for item in published_issues], []
    )
    most_recent_issue_published_at = max(item["date"] for item in published_issues)
    most_recent_issue_published_id = max(item["id"] for item in published_issues)

    with open(bookmarks_filename) as fh:
        existing_bookmarks = list(yaml.load_all(fh))

    bookmarks_for_new_issue = []
    for bookmark in existing_bookmarks:
        if len(bookmarks_for_new_issue) == MAX_BOOKMARKS_PER_ISSUE:
            break
        if bookmark["id"] in published_bookmark_ids:
            continue
        if any(tag in BLACKLISTED_TAGS for tag in bookmark["tags"]):
            continue

        # reality check
        bookmark_created_at = datetime.datetime.now() - bookmark["date"]
        assert bookmark_created_at < datetime.timedelta(days=180)

        bookmarks_for_new_issue.append(bookmark["id"])

    if len(bookmarks_for_new_issue) < MIN_BOOKMARKS_PER_ISSUE:
        raise RuntimeError(
            f"Could not gather more than {MIN_BOOKMARKS_PER_ISSUE} new bookmarks."
        )

    new_issue_dt = most_recent_issue_published_at + datetime.timedelta(
        days=DAYS_BETWEEN_ISSUES
    )
    # create a copy of new_issue_dt, otherwise ruamel.yaml will create an anchor
    new_issue_modifed_dt = new_issue_dt + datetime.timedelta(days=0)
    new_issue = {
        "id": most_recent_issue_published_id + 1,
        "date": new_issue_dt,
        "modified": new_issue_modifed_dt,
        "bookmark_ids": sorted(bookmarks_for_new_issue),
    }
    if new_issue["id"] in published_ids:
        raise RuntimeError("random issue id collision.")
    if new_issue["id"] != len(published_issues) + 1:
        raise RuntimeError("Issue number not equal to issues count.")
    with open(issues_filename, "w") as fh:
        yaml.dump_all(published_issues + [new_issue], fh)
    print(
        f"added new issue with {len(bookmarks_for_new_issue)} bookmark(s).",
        file=sys.stderr,
    )


if __name__ == "__main__":
    update_beyond_seven_review_issues()
