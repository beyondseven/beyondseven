AUTHOR = "Beyond Seven Review Authors"
SITENAME = "Beyond Seven Review"
SITESUBTITLE = "Book History, Bayesian Statistics, Information Science and Geisteswissenschaften"
SITEURL = ""

PATH = "content"

TIMEZONE = "Etc/UTC"

DEFAULT_LANG = "en"

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Allow longer summary
SUMMARY_MAX_LENGTH = 300

# Do not use Google fonts
USE_GOOGLE_FONTS = False

# Blogroll
LINKS = ()

# Social widget
SOCIAL = ()

DEFAULT_PAGINATION = 10

# Uncomment following line if you want document-relative URLs when developing
RELATIVE_URLS = True

THEME = "themes/Flex"
