# This file is only used if you use `make publish` or
# explicitly specify it as your config file.

import os
import sys

from pelicanconf import *

sys.path.append(os.curdir)

SITEURL = "https://www.beyondseven.org"

FEED_ALL_ATOM = "feeds/all.atom.xml"
CATEGORY_FEED_ATOM = "feeds/{slug}.atom.xml"

DELETE_OUTPUT_DIRECTORY = True

# `summary` has not yet been migrated to a Pelican 4.x compatible plugin
PLUGIN_PATHS = ['pelican-plugins']
PLUGINS = ["summary"]
