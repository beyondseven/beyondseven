import datetime
import os
import re

import click
import jinja2
import yaml

POST_FILENAME_TEMPLATE = "beyond-seven-review-issue-{:04d}.md"

# TOPIC_LABELS should be synced with blog/content/pages/about.md
TOPIC_LABELS = {
    "🌺": "Free/libre and open-source software 🌺",
    "🚟": "Book and publishing history 🚟",
    "⚽": "Data analysis and Bayesian statistics ⚽",
    "🏺": "Information and Geisteswissenschaften 🏺",
    "🌔": "Counterantidisintermediation 🌔",
    "🦓": "Scholarly communication 🦓",
    "🚣": "(Quantitative) cultural studies 🚣",
    "🦉": "(Quantitative) literary history and sociology of literature 🦉",
}
POST_TEMPLATE_JINJA2 = """
Title: Beyond Seven Review No. {{issue_number}}
Date: {{date}}
Category: Beyond Seven Review

<section>
{% for group in bookmarks|groupby("topic") %}

  <div class="topic">
  <h3 style="font-size: 1.1em;">{{ group.grouper }}</h3>

  <ul style="list-style-type: none; margin: 1rem; padding: 0.2rem;">
  {% for bookmark in group.list %}

    <li class="bookmark">
      <a class="bookmark_title" href="{{bookmark.url}}">{{bookmark.title}}</a>
      <blockquote class="comment">{{bookmark.comment}}</blockquote>
    </li>

  {% endfor %}
  </ul>

{% endfor %}
</section>

<!-- PELICAN_END_SUMMARY -->

<hr>

Beyond Seven Review appears regularly at <a
href="https://www.beyondseven.org">https://www.beyondseven.org</a>.
<a href="https://www.beyondseven.org/feeds/all.atom.xml">Subscribe</a>.

""".lstrip()


@click.command()
@click.argument("review_issues_filename")
@click.argument("bookmarks_filename")
@click.argument("output_path")
def generate_beyond_seven_review_posts(
    review_issues_filename, bookmarks_filename, output_path
):
    """DOCSTRING GOES HERE.

    Goal is to be idempotent.

    """
    with open(review_issues_filename) as fh:
        published_issues = list(yaml.safe_load_all(fh))
    with open(bookmarks_filename) as fh:
        bookmarks = list(yaml.safe_load_all(fh))

    for issue in published_issues:
        if issue["date"] > datetime.datetime.now():
            # post is scheduled for the future
            continue
        template = jinja2.Template(POST_TEMPLATE_JINJA2)
        first_day_of_week = datetime.datetime(
            issue["date"].year,
            issue["date"].month,
            issue["date"].day,
        ) - datetime.timedelta(days=issue["date"].weekday())
        first_day_of_week_human_readable = first_day_of_week.strftime("%-d %b %Y")
        issue_bookmarks = list(
            filter(lambda bookmark: bookmark["id"] in issue["bookmark_ids"], bookmarks)
        )
        assert len(issue_bookmarks) == len(
            issue["bookmark_ids"]
        ), "missing bookmark ids"

        # add topic to each bookmark. Topics are indicated by a special tag:
        # example: '!M-⚽-methods-data-analysis-bayesian-statistics'
        # the unicode identifier is what indicates the topic "⚽", the text
        # label is arbitrary and may change
        for bookmark in issue_bookmarks:
            for tag in bookmark['tags']:
                if re.match(r'^![A-Z]-.*', tag):
                    _, topic_id, _ = tag.split('-', 2)
                    bookmark['topic'] = TOPIC_LABELS[topic_id]
                    break
            assert bookmark.get('topic'), bookmark['tags']
        markdown = template.render(
            bookmarks=issue_bookmarks,
            date=issue["date"],
            first_day_of_week_human_readable=first_day_of_week_human_readable,
            issue_number=issue["id"],
        )
        filename = POST_FILENAME_TEMPLATE.format(issue["id"])
        with open(os.path.join(output_path, filename), "w") as fh:
            fh.write(markdown)


if __name__ == "__main__":
    generate_beyond_seven_review_posts()
