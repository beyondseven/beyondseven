# Beyond Seven Project Documentation 

## Focus of *Beyond Seven Review*

<!-- See [Editorial statement](editorial-statement.md). -->

*Beyond Seven Review* aims to feature links to content on the following
subjects (in no particular order):

- Bayesian methods for the analysis of culture
- Sozialgeschichte der Literatur
- comparative literature
- comparative text-based media studies
- quantitative literary and cultural analysis
- quantitative literary and cultural history
- sociologie du livre 
- sociology of literature

Links to non-recent, non-anglophone content are encouraged.

The focus of *Beyond Seven Review* will likely evolve. Short-term stability in
topics covered is, however, very much a goal.

## Contributing to *Beyond Seven Review*

Submissions of links to content likely to be of interest to readers of *Beyond
Seven Review* are accepted. Please use the project's issue tracker.

## How this works

The most important pieces of this project live in the ``source`` directory in
the root of the repository. Here's a rough overview of how things work:

- ``source/bookmarks.yaml`` and ``source/beyond-seven-review-issues.yaml`` are updated, triggering a build
- Build renders any new information to the blog.
- Periodically, other targets update based on updates to the blog's atom/rss feed.

Additional details may be found in [Adding New Entries](adding-new-entries.md).

## Potential targets

- rss -> email
- onion
- ipfs
- ssb
- mastodon
- matrix
- HN
- reddit  (appropriate subreddit?)
- birdsite
